Rails.application.routes.draw do
  mount Accelerator::Engine => '/accelerator', as: :accelerator
end

Accelerator::Engine.routes.draw do
  get '/', to: 'accelerator#status'
  post '/', to: 'accelerator#status'
  root to: 'accelerator#status'
end
