# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'accelerator/version'

Gem::Specification.new do |spec|
  spec.name          = "accelerator"
  spec.version       = Accelerator::VERSION
  spec.authors       = ["Greg Lappen"]
  spec.email         = ["greg@skyscraperstudios.com"]

  spec.summary       = 'Accelerates the app'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "rails", ">= 3.1"
  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "rake", "~> 10.0"
end
