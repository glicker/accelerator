require 'fileutils'

class AcceleratorController < ApplicationController

  skip_before_filter :authenticate_user!

  def status
    if request.post? && params[:command] == 'Lock'
      FileUtils.mkdir_p(File.dirname(Accelerator.brakefile_name))
      FileUtils.touch(Accelerator.brakefile_name)
      redirect_to accelerator.root_url
    elsif request.post? && params[:command] == 'Unlock'
      FileUtils.remove(Accelerator.brakefile_name) if File.exists?(Accelerator.brakefile_name)
      redirect_to accelerator.root_url
    else
      render layout: nil
    end
  end
end
