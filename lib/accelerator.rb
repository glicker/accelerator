require "accelerator/version"

module Accelerator
  class Middleware
    def initialize(app)
      @app = app
      Rails.logger.error("initializing middleware")
    end

    def call(env)
      if env['REQUEST_URI'].match(%r{/accelerator}) || !File.exists?(Accelerator.brakefile_name)
        @app.call(env)
      else
        [200, {'Content-Type' => 'text/html'}, ['Site is unavailable']]
      end
    end
  end

  class Engine < Rails::Engine
    initializer "accelerator.setup" do |app|
      app.middleware.use Accelerator::Middleware
      middleware.use Rack::Auth::Basic, "Restricted Area" do |username, password|
          [username, password] == ['admin', 'accelerator']
      end
    end
  end

  def self.brakefile_name
    File.join [Dir.tmpdir, Rails.application.class.parent.to_s.downcase, "0bd36bfd4d80867c0d67a43f7456bee5e5dbc7c1ff74da7b7b3add90f31e73f3.txt"]
  end
end

